from random import randint
from gasp import * 

begin_graphics()
finished = False
dead = False


def place_robot(robot_x,robot_y):
    global robot_shape
    robot_shape = Box((10*robot_x+5,10*robot_y+5),10,10,filled=False)
    return robot_shape

def place_player(player_x = randint(0,63),player_y = randint(0,47)):
    global player_shape
    print("here i am!")
    player_shape = Circle((10 * player_x + 5 , 10 * player_y +5), 5, filled=True)
    return player_shape

def move_player(player_x,player_y,dead):
    print("i am moveing")
    while dead == False:
        key = update_when("key_pressed")
        if key =="q":
            if player_x > 0 and player_y < 46:
                player_x += -1
                player_y += 1        
        elif key == "w":
            if player_y < 46:
                player_y += 1
        elif key == "e":
            if player_x < 63 and player_y < 46:
                player_x += 1
                player_y += 1
        elif key == "a":
            if player_x > 0:
                player_x += -1
        elif key == "s":
            player_x += 0 
        elif key == "d":
            if player_x < 63:
                player_x += 1
        elif key == "z":
            if player_x > 0 and player_y > 0:
                player_x += -1
                player_y += -1
        elif key == "x":
            if player_y > 0:
                player_y += -1
        elif key == "c":
            if player_y > 0 and player_x < 63:
                player_y += -1
                player_x += 1
        move_to(player_shape,(10*player_x+5,10*player_y+5))
    

def move_robot(robot_x,robot_y):
    if player_x > robot_x and player_y > robot_y:
        robot_x+=1
        robot_y+=1
        print(robot_x,robot_y)
    move_to(robot_shape,(10*robot_x+5,10*player_y+5))

robot_x = randint(0,63)
robot_y = randint(0,46)
player_x = randint(0,63)
player_y = randint(0,46)

place_player(player_x,player_y)
place_robot(robot_x,robot_y)

while not finished:
    move_player(player_x,player_y,dead)
    move_robot(robot_x,robot_y)

nd_graphics()
