# this took me forever
from turtle import *

def turtle_drawing(turtle1, turtle2, distance, angle):
    turtle1.left(angle)
    turtle2.right(angle)
    turtle1.forward(distance)
    turtle2.forward(distance)
    return distance

space = Screen()
t = Turtle()
t2 = Turtle()
print(turtle_drawing(t, t2, 100, 45))
