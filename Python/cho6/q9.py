def square(turtle,size=90):
    turtle.forward(size)
    turtle.right(size)
    turtle.forward(size)
    turtle.right(size)
    turtle.forward(size)
    turtle.right(size)
    turtle.forward(size)
    turtle.right(size)

from turtle import *    # use the turtle library
space = Screen()        # create a turtle screen
malik = Turtle()        # create a turtle named malik
square(malik)           # draw a square with malik

