def sum_list(num_list):
    total = 0               
    for n in num_list:      
         total+=n        
    return total

my_nums = range(1,11)
print(sum_list(my_nums))
