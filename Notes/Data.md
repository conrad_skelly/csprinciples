Big Idea 2: Data
Main ideas
Abstractions such as numbers, colors, text, and instructions can be represented by binary data ("It's all just bits!").
Numbers can be converted from one number system to another. Computers use the binary number system ("It's all just bits!" ;-).
Metadata helps us find and organize data.
Computers can process and organize data much faster and more accurately than people can.
Data mining, the process of using computer programs to sift through data to look for patterns and trends, can lead to new insights and knowledge.
It is important to be aware of bias in the data collection process.
Communicating information visually helps get the message across.
Scalability is key to processing large datasets effectively and efficiently.
Increasing needs for storage led to the development of data compression techniques, both lossless and lossy.
Vocabular:y
abstraction the process of removing elements of a code or program that aren't relevant or that distract from more important elements
analog data data that is represented in a physical way
bias systematic and repeatable errors in a computer system that create "unfair" outcomes because of the way a program was made witht the people who made it 
binary number system base 2 number system also a very common one used in computers
bit a binary digit, the smallest increment of data on a computer aka 1/0
byte a unit of data that is eight binary digits long
classifying data the process of organizing data into categories that make it easy to retrieve, sort and store for future use
cleaning data the process of fixing or removing incorrect, corrupted, incorrectly formatted, duplicate, or incomplete data within a dataset
digital data the electronic representation of information in a format or language that machines can read and understand
filtering data the process of choosing a smaller part of your data set and using that subset for viewing or analysis
information Information systems collect, store and supply data
lossless data compression a class of data compression algorithms that allows the original data to be perfectly reconstructed from the compressed data
lossy data compression the data in a file is removed and not restored to its original form after decompression
metadata the data providing information about one or more aspects of the data
overflow error when the data type used to store data was not large enough to hold the data
patterns in data a series of data that repeats in a recognizable way 
round-off or rounding error is the difference between an approximation of a number used in computation and its exact
scalability the measure of a system's ability to increase or decrease in performance and cost in response to changes in application and system processing demands

