

New technologies can have both beneficial and unintended harmful effects, including the presence of bias.

Citizen scientists are able to participate in identification and problem solving.
The digital divide keeps some people from participating in global and local issues and events.

Licensing of people’s work and providing attribution are essential.
We need to be aware of and protect information about ourselves online.

Public key encryption and multifactor authentication are used to protect sensitive information.

Malware is software designed to damage your files or capture your sensitive data, such as passwords or confidential information.


Vocabulary

Asymmetric ciphers: a prossecess of useing a relateed pair of keys one public and one privet to encrypt and decrypt a message and protect it form unauthorized access or use

authentication: the process of determining wether someone or something is who or what it says it is

Bias: prejudice in favor of or aganist one thing, person, or group compared with another, usally in a way considered to be unfair

Certificate Authority: (CA) is a trusted entity that issues secure sockets layer or (SSL) certificates

citizen science: the collection and analysis of data relating to the natural world by members of the general public, typically as part of a collaborative project with professional scientists

Creative Commons licensing: something that allows the creator to retain copyright whilst allowing others to copy and distribute and make use of their work non-commercially

crowdsourcing: the practice of obtaining information or input into a task or project by enlisting the services of a large number of people, either paid or unpiad

Cybersecurity: the state of being protected against the criminal or unauthorized use of electronic data, or the measures taken to achieve this

data mining: the practice of analyzing large databases in order to generate new information

decryption: The conversion of encrypted data into its original form

digital divide: the gulf between those who have ready access to computers and the internet, and those who do not

encryption: the process of converting information or data into a code, especially to prevent unauthorized access

intellectual property: the process of converting information or data into a code, especially to prevent unauthorized access

keylogging: the use of a computer program to record every keystroke made by a computer user, especially in order to gain fraudulent access to passwords and other confidential information

malware: software that is specifically designed to disrupt, damage, or gain unauthorized access to a computer system

multifactor authentication:a layered approach to securing data and applications where a system requires a user to present a combination of two or more credentials to verify a user's identity for login

open access: the unrestricted right or opportunity to use or benefit from something, in particular academic writing or research

open source: denoting software for which the original source code is made freely available and may be redistributed and modified.

free software: software that respects users' freedom and community

FOSS: Free and open-source software

PII: Any representation of information that permits the identity of an individual to whom the information applies to be reasonably inferred by either direct or indirect means

phishing: the fraudulent practice of sending emails or other messages purporting to be from reputable companies in order to induce individuals to reveal personal information, such as passwords and credit card numbers

plagiarism: the practice of taking someone else's work or ideas and passing them off as one's own

public key encryption: a method of encrypting or signing data with two different keys and making one of the keys, the public key, available for anyone to use

rogue access point: an access point installed on a network without the network owner's permission

targeted marketing: an approach to raise awareness for a product or service among a specific (targeted) group of audiences that are a subset of the total addressable market

virus: a piece of code that is capable of copying itself and typically has a detrimental effe
t, such as corrupting the system or destroying data
