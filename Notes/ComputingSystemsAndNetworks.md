# Big Idea 4: Computing Systems and Networks
## Main ideas
1. Built-in redundancy enables the Internet to continue working when parts of it are not operational.
2. Communication on the Internet is defined by protocol such as TCP/IP that determine how messages to send and receive data are formatted.
3. Data sent through the Internet is broken into packets of equal size.
4. The Internet is scalable, which means that new capacity can be quickly added to it as demand grows.
5. The World Wide Web (WWW) is a system that uses the Internet to share web pages and data of all types.
6. Computing systems can be in sequential, parallel, and distributed configurations as they process data.
## Vocabulary
1. Bandwidt a measure of the data transfer rate or capacity of a given networkh
2. Computing device A computing device can consist of a standalone unit or several interconnected units some examples are ipad and phone ect 
3. Computing network interconnected computing devices that can exchange data and share resources with each other
4. Computing system  a system of one or more computers and associated software with common storage
5. Data stream data that is generated continuously by thousands of data sources, which typically send in the data records simultaneously, and in small sizes (order of Kilobytes).
6. Distributed computing system the method of making multiple computers work together to solve a common problem
7. Fault-tolerant a set of recent techniques that were developed to increase plant availability and reduce the risk of safety hazards
8. Hypertext Transfer Protocol (HTTP) allows users to communicate data on the World Wide Web.
9. Hypertext Transfer Protocol Secure (HTTPS) a protocol that secures communication and data transfer between a user's web browser and a website
10. Internet Protocol (IP) address a unique address that identifies a device on the internet or a local network
11. Packets a small segment of a larger message
12. Parallel computing system the study, design, and implementation of algorithms in a way as to make use of multiple processors to solve a problem
13. Protocols a set of rules for formatting and processing data
14. Redundancy a system design in which a component is duplicated so if it fails there will be a backup
15. Router managing traffic between these networks by forwarding data packets to their intended IP addresses, and allowing multiple devices to use the same Internet connection
