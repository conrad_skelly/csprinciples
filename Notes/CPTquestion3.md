# Written Response (Question 3) for Create Performance Task

## 3a. Provide a written response that does all three of the following:

1. Describes the overall purpose of the program
>the overall purpose of this program is to run a game where you have to make the robots bump into ofther robots of pices of junk kill all the robots and you win the game
>
>
2. Describes what functionality of the program is demonstrated in the
   video.
> the robots case the player affter the player moves they will move it the most optimal derection aka the fastest way to get to the player when the robots crash into the ofther robots they die and produce a pice of junk if a robot crashes into another pice of junk then the robot will die  when all robots are dead the game ends and it prints you win
>
>
3. Describes the input and output of the program demonstrated in the
   video.
> the imput of the game is the players movment so if i give a input of w the player will move up once if i give a input of q the player will move up and to the left once etc 
>![image of movement](images/q3b1.png)
>

## 3b. Provide a written response to the following:
 
Capture and paste two program code segments you developed during the
administration of this task that contain a list (or other collection
type) being used to manage complexity in your program.

1. The first program code segment must show how data have been
   stored in the list.
>![data in a list](images/q3b1.png)
>
>
2. The second program code segment must show the data in the same list being
   used, such as creating new data from the existing data or accessing
   multiple elements in the list, as part of fulfilling the program’s purpose.
>![date n a list being used](/Users/1022892/Desktop/Screen\ Shot\ 2022-12-08\ at\ 3.23.38\ AM.png )
>
>

## Then provide a written response that does all three of the following: 

1. Identifies the name of the list being used in this response
>robots
>
>
2. Describes what the data contained in the list represent in your
   program
>this is were the robots are place so thier random position thier shape and wther they are junk or not
>
>
3. Explains how the selected list manages complexity in your program code by
   explaining why your program code could not be written, or how it would be
   written differently, if you did not use the list
>the list manages complexity because the number of robots on screen can be flexibal because of the list basicly you can easily add and take away robots useing a list and if you dident have a list and you wanted 5 robots on screen then you would need 5 different robot variabls giveing them each the same code instead of just useing one list
>
>

## 3c. Provide a written response to the following:

Capture and paste two program code segments you developed during the
administration of this task that contain a student-developed procedure that
implements an algorithm used in your program and a call to that procedure.

1. The first program code segment must be a student-developed
   procedure that
   - Defines the procedure’s name and return type (if necessary)
   - Contains and uses one or more parameters that have an effect on the
     functionality of the procedure
   - Implements an algorithm that includes sequencing, selection, and
     iteration
> ![the procedure][/Users/1022892/Desktop/Screen\ Shot\ 2022-12-08\ at\ 3.33.48\ AM.png]
>
>
>
>
2. The second program code segment must show where your student-developed
   procedure is being called in your program.
>![the pricedure being called][/Users/1022892/Desktop/Screen\ Shot\ 2022-12-08\ at\ 3.35.33\ AM.png]
>
>
>

## Then provide a written response that does both of the following:

3. Describes in general what the identified procedure does and how it
   contributes to the overall functionality of the program
>the pocedure checks if the of one thing == the x of another thing and the y of one thing == the y of another thing and then returns True or False it contributs to the funtionality of the program by makeing it that if it returns True the game will end
>
>
>
4. Explains in detailed steps how the algorithm implemented in the identified
   procedure works. Your explanation must be detailed enough for someone else
   to recreate it.
>the prosigoure will take two peramiters thing1 and list of things we will make a for loop that gose through list of things and we will check if our peramiters x == thing2 or whaterver variable you used and we will check to see if our y is ==  to thing2 or whatever variable you use if it is both True so make sure to use and return Ture if its not Ture return False
>
>
>

## 3d. Provide a written response that does all three of the following:

1. Describes two calls to the procedure identified in written response 3c. Each
   call must pass a different argument(s) that causes a different segment of
   code in the algorithm to execute.
>
> First call:lets say it gests the players position and the robots positions/junk and sees if the player is tuching it if so then it will return true 
>
>
>
>
> Second call: now lets say the player and the robot are not touching it will return false
>
>
>
>
2. Describes what condition(s) is being tested by each call to the procedure
>
> Condition(s) tested by the first call: if player x and y is equal to a robot or  junk
>
>
>
>
> Condition(s) tested by the second call: if player x and y is quale to a robot or a junk
>
>
>
>
3. Identifies the result of each call
>
> Result of the first call:
>if it returns ture then finished will = ture and a text will apper sayiing youu have been cought and then game will end
>
>
>
> Result of the second call: it returns false and nothing happends as a effect of it returning false the game gose on
>
>
>
>
